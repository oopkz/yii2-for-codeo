<?php
return [
    'sourcePath' => __DIR__. DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR,
    // 'languages' => ['ru', 'kz', 'en'], // аударылу керек тілдерді массив түрде қөсыңыз
    'languages' => array_keys(Yii::$app->params['langs']), // аударылу керек тілдерді массив түрде қөсыңыз
    'translator' => 'Yii::t',
    'sort' => false,
    'removeUnused' => false,
    'only' => ['*.php'],
    // мында көрсетіген папкаларға кірмейді (соның ішіндегі создерді аудармайды)
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/vendor',
    ],
    'format' => 'php',
    'messagePath' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'messages',
    'overwrite' => true,
];