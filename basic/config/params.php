<?php

return [
    'adminEmail' => 'admin@example.com',

    'defaultLang' => 'kz',
    
    'langs' =>[
    	'en' => 'English',
    	'kz' => 'Қазақша',
    	'ru' => 'Русский',
    ],
];
