<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class CompanyController extends Controller
{

    /**
     * мекемелер туралы
     *
     * @return string
     */
    public function actionIndex()
    {
        $id = rand();
        return $this->render('index',['id' => $id]);
    }

    /**
     * мекеме туралы
     *
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view',    ['id' => $id]);
    }
}
