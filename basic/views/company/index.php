<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'codeo.kz:'.Yii::t('app', 'Мекемелер');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

	<div class="alert alert-success">
	<strong>Html және Url арқылы қүрылған сілтмелер (дүрыс)</strong>
	<p>		
		<?= Html::a(Yii::t('app', "Толығырақ"),['/company/view', 'id' => $id])?>
		<br />
		<a href="<?= Url::to(["/company/view", "id" => $id]) ?>"><?= Yii::t('app', "Толығырақ") ?></a>
	</p>

	</div>
		
	<div class="alert alert-danger">
	<strong><?= Yii::t('app', "қате қүрылған сілтмелер") ?> </strong>
	<p>		
		<a href="/company/view?id=<?= $id ?>">Толығырақ</a>
	</p>
	</div>
</div>
