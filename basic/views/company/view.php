<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Мекеме түралы').': ' . $id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	<p><?= Yii::t('app', 'Мекеме түралы') ?></p>
	<p><?= Yii::t('app', 'Нөмері {id} мекеме Абай даңғылында орналасқан', ["id" => $id]) ?></p>
</div>
