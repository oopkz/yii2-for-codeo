<?php

namespace app\components;

use yii\base\Component;
use yii\base\BootstrapInterface;
use Yii;

class LangManager extends Component implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $request = $app->request;

        // проверяем наличие языков сайта
        $lang_array = array_keys($app->params['langs']);

        $default_lang = $app->params['defaultLang'];
        if (!isset($lang_array) or !isset($default_lang))
        {
        	echo "Не удалось определить язык сайта!";
        	die();
        }

        // парсим запрос пользователя
        $parseResult = Yii::$app->urlManager->parseRequest($app->request);
        
        //---------------------------------------------------------
        // этот блок не направляет, при использовании дебага
        if (isset($parseResult[0]) && $parseResult[0] == 'debug'){
            return true;
        }
        if (isset($parseResult[1]['language']) && $parseResult[1]['language'] == 'debug'){
            return false;
        }
        //---------------------------------------------------------

        if (isset($parseResult[1]['language']))
        {
        	$site_lang = $parseResult[1]['language'];
        	
        	if (in_array($site_lang, $lang_array))
        	{
        		$app->language = $site_lang;
        	}else
        	{
        		// идет перенаправление на язык по умолчанию
       			return $app->getResponse()->redirect("/".$default_lang."/");
        	}
        }else
        {
        	// идет перенаправление на язык по умолчанию
       		return $app->getResponse()->redirect("/".$default_lang."/");        	
        }
        
    }
}