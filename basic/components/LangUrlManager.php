<?php 
namespace app\components;
use yii\web\UrlManager;
use Yii;

class LangUrlManager extends UrlManager
{
    public function createUrl($params)
    {
        $params['language']=Yii::$app->language;
        return parent::createUrl($params);
    }
}